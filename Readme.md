# Soal 1: Sistem register dan login

## 1.1 Membuat sistem register dan login

Membuat sistem register pada script `register.sh` dan setiap user yang berhasil didaftarkan disimpan di dalam file `./users/user.txt` dan sistem login yang dibuat di script `main.sh`.

Pada kedua script `register.sh` dan `main.sh`, langkah pertama kita perlu menerima input password dan username dari pengguna. Adapun perintahnya sebagai berikut:

```bash
echo "masukkan username"
read username
echo "masukkan password"
read -s password
```

Kemudian dilakukan cek apakah file `./users/user.txt` sudah dibuat apa belum. Apabila belum dibuat maka directory dan file dibuat terlebih dahulu menggunakan perintah

```bash
if [ ! -d "./users/" ]
then
    echo `mkdir users`
    echo `touch ./users/user.txt`
fi
```

## 1.2 Kriteria Password

Input password pada login dan register harus
tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut:

i. Minimal 8 karakter

ii. Memiliki minimal 1 huruf kapital dan 1 huruf kecil

iii. Alphanumeric

iv. Tidak boleh sama dengan username

Supaya password pada login dan register tertutup/hidden, maka pada saat proses input password dari pengguna perlu mengunakan perintah `read -s password`. ` -s` digunakan untuk membaca kata sandi karena akan membaca kata sandi dalam _silent mode_.

Kemudian kita membuat function `cek()` untuk mengecek setiap input password dari pengguna agar memenuhi kriteria password yang diberikan.

```bash
cek(){
    # flag
    minChar=true
    upLoCase=false
    numeric=false
    likeUsername=false

    # Untuk password minimal 8 karakter
    if [ ${#password} -lt 8 ]
    then
        echo "character must be more 8!"
        minChar=false
    fi
    # Untuk password memiliki minimal 1 huruf kapital dan 1 huruf kecil
    if [[ $password =~ [A-Z]  &&  $password =~ [a-z] ]]
    then
        upLoCase=true
    else
        echo "character must include at least 1 UPPERCASE and lowercase!"
    fi
    # Untuk password Alphanumeric
    if [[ $password =~ [0-9] ]]
    then
        numeric=true
    else
        echo "character must include at least 1 Alphanumeric!"
    fi
    # Untuk password tidak boleh sama dengan username
    if [ "$username" == "$password" ]
    then
        echo "username and password must be not same"
        likeUsername=true
    fi
}
```

Menggunakan _conditional statement_ `if..else`, untuk mengecek apakah password sudah sesuai kriteria atau tidak. Apabila sudah sesuai maka set flag menjadi `true`, jika tidak maka diberi pesan peringatan. Fungsi `cek()` tersebut dipanggil setelah proses input username dan password dari pengguna dilakukan.

## 1.3 Pesan pada log

Setiap percobaan login dan register akan tercatat pada `log.txt` dengan format : `MM/DD/YY hh:mm:ss MESSAGE`.

Seperti pada soal 1.1 langkah pertama perlu dilakukan pengecekan apakah file `log.txt` sudah dibuat atau tidak.

```bash
if [ ! -f "~/log/log.txt" ]
then
    echo `touch ~/log/log.txt`
fi
```

Kemudian didapatkan tanggal dan waktu percobaan login/register untuk keperluan format log menggunakan perintah

```bash
date=`date +%D`
time=`date +%T`
```

Message pada log akan berbeda tergantung aksi
yang dilakukan user.

### 1.3.1 Percobaan register dengan username yang sudah terdaftar

format pesan pada log : `REGISTER: ERROR User already exists`

```bash
temp=`cat ./users/user.txt`

counter=0
for i in $temp
do
    if [ $(($counter%2)) -eq 0 ]
    then
        # echo "$i and $username"
        if [ "$username" == "$i" ]
        then
            likeUsername=true
            echo "$date $time REGISTER: ERROR User already exist" >> ~/log/log.txt
            # echo "MASUK"
        fi
    fi
    # echo $counter
    counter=$((counter+1))
done
```

Menggunakan perulangan for untuk setiap baris dalam file `user.txt` yang disimpan dalam variabel `temp` kemudian dicek apabila username inputan pengguna sama dengan username yang tersimpan dalam file, maka tampilkan pesan sesuai format dan set flag `likeUsername=true` supaya proses register tidak dilanjutkan.

Karena format penyimpanan username dan password dalam file `user.txt` seperti pada contoh dibawah

```
Rama            \\username
Rama12345       \\password
Isol            \\username
Isol12345       \\password
```

Sehingga untuk meminimalisir perulangan setiap baris, maka baris yang dicek hanya baris untuk password atau baris ganjil saja (baris dimulai dari 0).

```bash
    if [ $(($counter%2)) -eq 0 ]
    then
        ...
    fi
```

### 1.3.2 Percobaan register berhasil

format pesan pada log : `REGISTER: INFO User USERNAME registered successfully`

```bash
if [[ "$minChar" = true && "$upLoCase" = true && "$numeric" = true && "$likeUsername" = false ]]
then
    echo "$date $time REGISTER: INFO User $username registered successfully" >> ~/log/log.txt
    echo -e "$username\n$password" >> ./users/user.txt
fi
```

Percobaan register berhasil ketika semua kriteria password terpenuhi atau ketika flag `minChar`, `upLocase`, dan `numeric` = `true` dan `likeUsername` = `false`

Tampilkan pesan percobaan sesuai format kemudian username dan password dimasukkan ke file `user.txt` menggunakan operator redirect `>>`.

```bash
echo -e "$username\n$password" >> ./users/user.txt
```

### 1.3.3 Percobaan login namun password salah

format pesan pada log : `LOGIN: ERROR Failed login attempt on user USERNAME`

Pertama kita bahas proses login pada script `main.sh`

```bash
isUserExist=false
userIndex=-2
isPassExist=false
passIndex=-2
accepted=false

temp=`cat ./users/user.txt`
counter=0
for i in $temp
do
    if [ $(($counter%2)) -eq 0 ]
    then
        if [ "$username" == "$i" ]
        then
            isUserExist=true
            userIndex=$counter
        fi
    fi

    if [ $(($counter%2)) -eq 1 ]
    then
        if [ "$password" == "$i" ]
        then
            isPassExist=true
            passIndex=$counter
        fi
    fi
    # echo "pass index = $passIndex and $userIndex "
    if [ $passIndex == $(($userIndex+1)) ]
    then
        accepted=true
    fi
    counter=$((counter+1))
done
```

Untuk proses login hampir sama dengan pengecekan kriteria password pada script `register.sh` dimana dilakukan perulangan for untuk setiap baris pada file `user.txt` yang mengandung username dan password. proses pengecekan password dilakukan pada baris ganjil dan username pada baris genap.

```bash
    # Untuk username
    if [ $(($counter%2)) -eq 0 ]
    then
        ...
    fi
    # Untuk password
    if [ $(($counter%2)) -eq 1 ]
    then
        ...
    fi
```

Apabila username dan password inputan pengguna sama dengan username dan password pada file `user.txt` maka set flag `isUserExist` dan `isPassExist` = `true`. Kemudian catat index username dan password (`userIndex`, `passIndex`).

Index berfungsi untuk mengecek apakah password tersebut dimiliki oleh username yang sama. Dapat dilakukan dengan perintah berikut

```bash
    if [ $passIndex == $(($userIndex+1)) ]
    then
        accepted=true
    fi
```

Kembali lagi ke proses percobaan login namun password salah.

Untuk dapat mendeteksi hal tersebut cukup dengan menggunakan variable `accepted` yang sudah di set `true` apabila proses login berhasil. Sehingga ketika `accepted=false` maka dapat dilakukan proses penampilan pesan pada log.

```bash
if [ "$accepted" == true ]
then
    ...
    fi
# else if [ "$isUserExist" == true ]
# di soal tidak ada opsi jika belum daftar dan mau login, jadi langsung else saja
else
    echo "$date $time LOGIN: ERROR Failed login attempt on user $username" >> ~/log/log.txt
fi
```

### 1.3.4 Percobaan login berhasil

format pesan pada log : `LOGIN: INFO User USERNAME logged in`

```bash
if [ "$accepted" == true ]
then
    echo "$date $time LOGIN: INFO User $username logged in" >> ~/log/log.txt
    ...
else
    ...
fi
```

Login berhasil ketika `accepted=true`. Proses penampilan pesan pada log bisa dilakukan

Untuk setiap pesan pada proses percobaan register dan login, pesan dimasukkan ke file `log.txt` menggunakan operator redirect `>>` seperti pada contoh dibawah.

```bash
echo "$date $time LOGIN: INFO User $username logged in" >> ~/log/log.txt
```

## 1.4 User Command

Setelah login, user dapat mengetikkan 2 command sebagai berikut:

### 1.4.1 `dl N` (N = Jumlah gambar yang akan di download)

Untuk proses mendownload gambar terdapat pada function `downloadImage()`. Terdapat dua kondisi pada command ini, yaitu ketika sudah terdapat file zip dan tidak.

Pertama kita perlu mendapatkan tanggal dan nama folder sesuai format yang ditentukan.

```bash
local date=`date +%F`
local folder=$date"_"$username
```

Kemudian dilakukan pengecekan apakah file zip sudah ada atau tidak.

Untuk kondisi file zip belum tersedia, maka perlu dibuat terlebih dahulu directory sesuai dengan format nama folder yang ditentukan. Kemudian dilakukan perulangan hingga N kali untuk mendownload gambar. Terdapat kondisi untuk format penamaan file gambar, yaitu untuk gambar kurang dari 10 angka diawali dengan 0. Untuk mendownload gambar dapat menggunakan command `wget`. Folder kemudian di `zip` dengan password sesuai dengan password saat login dan folder yang sebelumnya dibuat kemudian dihapus dengan menggunakan command `rm`.

```bash
if [ ! -f ./$folder.zip ]
    then
        echo `mkdir $folder`
        for ((num=1; num<=$1; num=num+1))
        do
            # echo $num
            if [ $num -lt 10 ]
            then
                local direktori="./$folder/PIC_0$num.jpg"
            else
                local direktori="./$folder/PIC_$num.jpg"
            fi
            # echo $direktori
            wget https://loremflickr.com/320/240 -O $direktori
        done
        echo `zip --password $password $folder.zip -r $folder/`
        echo `rm -r ./$folder`
```

Untuk kondisi file zip sudah tersedia, langkah pertama yang harus dilakukan adalah meng-`unzip` file zip tersebut kemudian dihapus menggunakan `rm`. Sebelum melanjutkan ke proses download gambar, dilakukan perhitungan gambar yang sudah terdapat dalam file zip tadi untuk melanjutkan penomoran format file gambar (menggunakan `ls` dan `wc`).

```bash
number=`ls $folder | wc -l`
```

Dilanjutkan proses download gambar sama seperti sebelumnya, folder di `zip`, dan dihapus.

```bash
else
        echo `unzip -P $password $folder.zip`
        echo `rm -r $folder.zip`

        number=`ls $folder | wc -l`
        for ((num=1+number; num<=$1+number; num=num+1))
        do
            # echo $num
            if [ $num -lt 10 ]
            then
                local direktori="./$folder/PIC_0$num.jpg"
            else
                local direktori="./$folder/PIC_$num.jpg"
            fi
            # echo $direktori
            wget https://loremflickr.com/320/240 -O $direktori
        done
        echo `zip --password $password $folder.zip -r $folder/`
        echo `rm -r $folder`
    fi
```

### 1.4.2 `att`

Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari
user yang sedang login saat ini.

Proses terdapat pada function `countInfoLogin()`.

Langkang yang digunakan cukup sederhana dimana dilakukan perulangan untuk setiap baris pada file `log.txt` untuk menghitung jumlah baris yang merepresentasikan jumlah percobaan login dilakukkan.

```bash
countInfoLogin(){
    local data=`cat ~/log/log.txt`
    local count=0
    while read line; do
        count=$((count+1))
    done < ~/log/log.txt
    echo "Percobaan Login $count"
}
```

## 1.5 Test Case/Percobaan

- Percobaan register dengan password tidak memenuhi kriteria

![register_gagal](foto/1.1.png)

- Percobaan login sukses

![login_sukses](foto/1.2.png)

- `user.txt`

![user.txt](foto/1.3.png)

- Command `dl 5`

![dl](foto/1.4.1.png)
![dl](foto/1.4.2.png)

- Command `att`

![att](foto/1.5.png)

- `log.txt`

![log](foto/1.6.png)

## 1.6 Kesulitan

Adapun kesulitan yang kami hadapi ketika mengerjakan soal ini yaitu:

1. membutuhkan waktu yang relatif lama untuk mendapatkan cara menzip dan unzip suatu file dengan password.
2. File log harus benar-benar terisolasi, dengan kata lain tidak boleh ada file lain selain untuk soal no.1. Jadi cukup merepotkan ketika mendebugging dan menemukan akar masalahnya saat itu mengenai folder harus terisolasi.
3. Masih belum terbiasa dengan syntax bash karena ada beberapa aturan yang sangat berbeda dengan bahasa pemrograman lainnya.

# Soal 2: Membaca log website `https://daffa.info`

Membuat sebuah script AWK bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:

## 2.1 Buat folder `forensic_log_website_daffainfo_log`

Dilakukan pengecekan apakah folder sudah ada atau tidak. Jika belum ada maka folder dibuat menggunakan command `mkdir`.

```bash
if [ ! -f "~/log/forensic_log_website_daffainfo_log" ]
then
    echo `mkdir ~/log/forensic_log_website_daffainfo_log`
fi
```

## 2.2 Rata-rata request per jam

Masukkan jumlah rata-ratanya ke dalam sebuah file bernama `ratarata.txt` ke dalam folder yang sudah dibuat.

Menggunakan AWK untuk membaca setiap baris dalam file `log_website_daffainfo.log`

```bash
total=`awk {print} ~/log/log_website_daffainfo.log`
```

Kemudian hasil AWK yang disimpan di dalam variabel total dihitung jumlah barisnya menggunakan `wc -l` untuk menghitung total request.

```bash
total=$((`echo "$total" | wc -l`-1))
```

Menghitung nilai rata-rata dengan rumus `total/12`. Nilai 12 didapatkan karena terdapat 12 jam yang terdapat request serangan. Hasil nilai rata-rata kemudian dimasukkan ke file `ratarata.txt` sesuai format yang ditentukan menggunakan operator redirect `>>`.

```bash
mean=$(($total/12))
echo "Rata-rata serangan adalah sebanyak $mean requests per jam" >> ~/log/forensic_log_website_daffainfo_log/ratarata.txt
```

## 2.3 IP yang paling banyak melakukan request ke server

```bash
soalCIP=`awk -F ':' '$1 {print $1}' ~/log/log_website_daffainfo.log | sort | uniq -c | sort -rn | awk '{print $2}' | awk 'NR==1'`
soalCJumlah=`awk -F ':' '$1 {print $1}' ~/log/log_website_daffainfo.log | sort | uniq -c | sort -rn | awk '{print $1}' | awk 'NR==1'`

echo "IP yang paling banyak mengakses server adalah: $soalCIP sebanyak $soalCJumlah requests" >> ~/log/forensic_log_website_daffainfo_log/result.txt
```

Menggunakan AWK dengan delimeter `:` untuk menyeleksi IP request. Kemudian hasil AWK diurutkan menggunakan `sort` dan dilakukan eliminasi terhadap IP duplikat dan dihitung banyaknya duplikasi menggunakan `uniq -c`. Lalu diurutkan kembali berdasarkan jumlah menggunakan `sort -rn`.

Dengan menggunakan AWK, untuk variabel `soalCIP` menampilkan IP dan `soalCJumlah` menampilan count/jumlah. Masing-masing variabel hanya ditampilan baris paling atas/pertama menggunakan `awk 'NR==1'` yang mereprentasikan IP yang paling banyak melakukan request.

IP dan banyak request yang didapatkan dimasukkan ke file `result.txt` sesuai dengan format yang ditentukan menggunakan operator redirect `>>`.

## 2.4 Requests yang menggunakan user-agent **curl**

Menggunakan AWK sederhana untuk menyeleksi requset yang menggunakan user-agent `curl`

```bash
countCurl=`awk '/curl/ {print}' ~/log/log_website_daffainfo.log | wc -l`
```

Jumlah request yang didapatkan dimasukkan ke file `result.txt` sesuai dengan format yang ditentukan menggunakan operator redirect `>>`.

```bash
echo "Ada $countCurl requests yang menggunakan curl sebagai user-agent" >> ~ log/forensic_log_website_daffainfo_log/result.txt
```

## 2.5 IP yang mengakses website pada jam 2 pagi pada tanggal 23

```bash
soalE=`awk '($1 ~ /2022:02:/)' ~/log/log_website_daffainfo.log | awk -F ':' '{print $1}' | uniq -c | awk '{print $2}'`
```

Menggunakan AWK untuk menyeleksi IP yang mengakses website pada jam tersebut. Kemudian baris yang terduplikasi dihilangkan menggunakan `uniq -c` dan ditampilkan IP-nya saja (`awk '{print $2}'`).

```bash
for i in $soalE
do
    echo $i
    echo "$i Jam 2 pagi" >> ~/log/forensic_log_website_daffainfo_log/result.txt
done
```

Menggunakan perulangan `for` untuk setiap IP yang didapatkan untuk ditampilkan dan dimasukkan ke file `result.txt` sesuai dengan format yang ditentukan menggunakan operator redirect `>>`.

## 2.6 Hasil

Berikut adalah isi dari file `ratarata.txt` dan `result.txt` setelah menjalankan script `soal2_forensic_dapos.sh`.

- `ratarata.txt`

![ratarata.txt](foto/2.1.png)

- `result.txt`

![result.txt](foto/2.2.png)

## 2.7 Kesulitan

Adapun kesulitan yang kami hadapi ketika mengerjakan soal ini yaitu:

1. Masih belum terbiasa dengan syntax awk
2. Cukup kesulitan untuk mencari cara menemukan suatu pola yang hanya dibedakan dengan ' : ', ' , ', ataupun ' . '.
3. Kesulitan menentukan variable pembagi rata-rata karena tidak ada penjelasan kusus mengenai hal itu sehingga pada awalnya kami berspekulasi bahwa rata-rata dihitung berdasarkan tiap-tiap jam yang terdapat penyerangan (13). Namun diberikan kemakluman dan toleransi oleh asdos penguji.

<!-- RIDWAN -->

# Soal 3

## 3.1 Membuat log file untuk penyimpanan metrics

Membuat log file di `minutes_log.sh` dengan format {YmdHms}. Contoh, Jika script file dijalankan pada waktu 2022-01-31 15:00:00 maka format nama file tersebut akan tergenerate menjadi `metrics_20220131150000.log`

```bash
day=`date +%d`
month=`date +%m`
year=`date +%Y`
hour=`date +%H`
minute=`date +%M`
second=`date +%S`

nameFile=$day$month$year$hour$minute$second
```

Dilakukan pengecekan file directory terlebih dahulu. Jika belum ada, maka akan dibentuk file directory nya.

```bash
if [ ! -f "~/log/metrics_$nameFile.log" ]
then
    echo `touch ~/log/metrics_$nameFile.log`
fi
```

## 3.2 Program Monitoring RAM dan Memory

Untuk pembuatan program monitoring resource terdapat beberapa command khusus. Untuk pencatatan RAM digunakan `free -m` dan untuk disk digunakan `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

```bash
memori=`free -m ~/`
echo $memori
titleMem="mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
dataMem=""
count=0
for i in $memori
do
    if [[ $count -gt 6 && $count -le 12 || $count -ge 14 ]]
    then
        dataMem=$dataMem$i","
    fi
    count=$(($count+1))
done
```

```bash
user=`whoami`
dataMem=$dataMem
echo $dataMem
disk=`du -sh ~/`
count=0
for i in $disk
do
    if [ $count == 0 ]
    then
        path=$i
    else
        diskData=$i
    fi
    # echo $i
    count=$(($count+1))
done
```

Echo untuk print out keterangannya. Perulangan-perulangan di atas adalah untuk mendapatkan data dari beberapa variabel pada Memory dan Storage. Data akan diteruskan ke directory berikut.

```bash
dataMem=$dataMem$diskData','$path
echo $titleMem >> ~/log/metrics_$nameFile.log
echo $dataMem >> ~/log/metrics_$nameFile.log
```

## 3.3 Menjalankan minutes_log.sh

Dikarenakan pada soal diminta untuk menjalankan script `minutes_log.sh` untuk setiap menit, Dibuatkan command pada cronjob pada crontab sebagai berikut.

```bash
* * * * * /home/ram/Sisop/modul\ 1/praktikum/soal\ 3/minute_log.sh
```

## 3.4 Membuat agregasi file log dalam satuan jam

Di dalam `aggregate_minutes_to_hourly_log.sh` kita akan memprint hasil minimum, maksimum, dan rata-rata dari tiap log yang berjalan per menitnya dalam durasi 1 jam.

## 3.5 Mencari Data Minimal, Maksimal, dan Rata-Rata

Pada program agregasi, untuk mencari nilai maksimal befokus pada baris kedua dan mengambil semua file pada folder log dengan jaminan folder log sudah terisolasi. Setiap data dipisahkan dengan ' , '. Oleh karena itu pada program awk mengambil tiap-tiap data setelah tanda koma.

```bash
titleLine=`awk '{print $0}' ~/log/* | awk 'NR % 2 == 1' | awk 'NR==1'`
titleLine="type,"$titleLine temp="awk '{print \$0}' ~/log/* | awk 'NR % 2 == 0'"
dataLine=`eval $temp`
totalData=`awk -F ',' '\$1 {print \$1}' ~/log/* | awk 'NR % 2 == 0' | wc -l`
```

Setelah mendapatkan data, kemudian tiap angka setelah koma masing masing menunjukkan

```
"mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
```

Dari rincian keterangan dan data di atas dicari nilai maksimum, minimum, dan average pada tiap-tiap file. Berikut codenya

```bash
data1="awk -F ',' '\$1 {print \$1}' ~/log/* | awk 'NR % 2 == 0'"
maxData1=`eval $data1 | sort -rn | awk 'NR==1'`
minData1=`eval $data1 | sort -n | awk 'NR==1'`
sumData1=`eval $data1 | awk '{ sum += $1 } END { print sum }'`

data2="awk -F ',' '\$1 {print \$2}' ~/log/* | awk 'NR % 2 == 0'"
maxData2=`eval $data2 | sort -rn | awk 'NR==1'`
minData2=`eval $data2 | sort -n | awk 'NR==1'`
sumData2=`eval $data2 | awk '{ sum += $1 } END { print sum }'`

data3="awk -F ',' '\$1 {print \$3}' ~/log/* | awk 'NR % 2 == 0'"
maxData3=`eval $data3 | sort -rn | awk 'NR==1'`
minData3=`eval $data3 | sort -n | awk 'NR==1'`
sumData3=`eval $data3 | awk '{ sum += $1 } END { print sum }'`

data4="awk -F ',' '\$1 {print \$4}' ~/log/* | awk 'NR % 2 == 0'"
maxData4=`eval $data4 | sort -rn | awk 'NR==1'`
minData4=`eval $data4 | sort -n | awk 'NR==1'`
sumData4=`eval $data4 | awk '{ sum += $1 } END { print sum }'`

data5="awk -F ',' '\$1 {print \$5}' ~/log/* | awk 'NR % 2 == 0'"
maxData5=`eval $data5 | sort -rn | awk 'NR==1'`
minData5=`eval $data5 | sort -n | awk 'NR==1'`
sumData5=`eval $data5 | awk '{ sum += $1 } END { print sum }'`

data6="awk -F ',' '\$1 {print \$6}' ~/log/* | awk 'NR % 2 == 0'"
maxData6=`eval $data6 | sort -rn | awk 'NR==1'`
minData6=`eval $data6 | sort -n | awk 'NR==1'`
sumData6=`eval $data6 | awk '{ sum += $1 } END { print sum }'`

data7="awk -F ',' '\$1 {print \$7}' ~/log/* | awk 'NR % 2 == 0'"
maxData7=`eval $data7 | sort -rn | awk 'NR==1'`
minData7=`eval $data7 | sort -n | awk 'NR==1'`
sumData7=`eval $data7 | awk '{ sum += $1 } END { print sum }'`

data8="awk -F ',' '\$1 {print \$8}' ~/log/* | awk 'NR % 2 == 0'"
maxData8=`eval $data8 | sort -rn | awk 'NR==1'`
minData8=`eval $data8 | sort -n | awk 'NR==1'`
sumData8=`eval $data8 | awk '{ sum += $1 } END { print sum }'`

data9="awk -F ',' '\$1 {print \$9}' ~/log/* | awk 'NR % 2 == 0'"
maxData9=`eval $data9 | sort -rn | awk 'NR==1'`
minData9=`eval $data9 | sort -n | awk 'NR==1'`
sumData9=`eval $data9 | awk '{ sum += $1 } END { print sum }'`

data10=`awk -F ',' '\$1 {print $10}' ~/log/* | awk 'NR % 2 == 0' | awk 'NR == 1'`

# perlu diolah
data11="awk -F ',' '\$1 {print \$11}' ~/log/* | awk 'NR % 2 == 0'"
# data11=`awk -F ',' '$1 {print $11}' ~/log/* | awk 'NR % 2 == 0'`
maxData11=`awk -F ',' '$1 {print $11}' ~/log/* | awk 'NR % 2 == 0' | sort -rn | awk 'NR==1'`
minData11=`awk -F ',' '$1 {print $11}' ~/log/* | awk 'NR % 2 == 0' | sort -n | awk 'NR==1'`
sumData11=`awk -F ',' '$1 {print $11}' ~/log/* | awk 'NR % 2 == 0' | awk '{ sum += $1 } END { print sum }'`
commData11=`awk -F ',' '\$1 {print \$11}' ~/log/* | awk 'NR % 2 == 0'`

data12="awk -F ',' '\$1 {print \$12}' ~/log/* | awk 'NR % 2 == 0'"
# commData12=
maxData12=`eval $data12 | sort -rn | awk 'NR==1'`
minData12=`eval $data12 | sort -n | awk 'NR==1'`
commData12=`eval $data12`


# gabungkan data 11 dan 12
# data13=`awk -F ',' '$1 {print $11"."$12}' ~/log/* | awk 'NR % 2 == 0'`
minData13=`awk -F ',' '$1 {print $11"."$12}' ~/log/* | awk 'NR % 2 == 0' | sort -n | awk 'NR==1'`
maxData13=`awk -F ',' '$1 {print $11"."$12}' ~/log/* | awk 'NR % 2 == 0' | sort -rn | awk 'NR==1'`
sumData13=`awk -F ',' '$1 {print $11"."$12}' ~/log/* | awk 'NR % 2 == 0' | grep -o '[0-9, "."]\+' | awk '{ sum += $1 } END { print sum }'`

# rerata
avgData1=$(($sumData1/$totalData))
avgData2=$(($sumData2/$totalData))
avgData3=$(($sumData3/$totalData))
avgData4=$(($sumData4/$totalData))
avgData5=$(($sumData5/$totalData))
avgData6=$(($sumData6/$totalData))
avgData7=$(($sumData7/$totalData))
avgData8=$(($sumData8/$totalData))
avgData9=$(($sumData9/$totalData))
avgData13=$(($sumData13/$totalData))

```

Kemudian tiap-tiap dari data dimasukkan ke dalam file aggregat sesuai dengan ketentuan. Berikut codenya.

```bash
day=`date +%d`
month=`date +%m`
year=`date +%Y`
hour=`date +%H`
minute=`date +%M`
second=`date +%S`

nameFile=$day$month$year$hour$minute$second
nameFile="metrics_agg_"$nameFile".log"
echo $titleLine >> ~/log/$nameFile
echo "minimum,"$minData1","$minData2","$minData3","$minData4","$minData5","$minData6","$minData7","$minData8","$minData9","$data10","$minData13 >> ~/log/$nameFile
echo "maximum,"$maxData1","$maxData2","$maxData3","$maxData4","$maxData5","$maxData6","$maxData7","$maxData8","$maxData9","$data10","$maxData13 >> ~/log/$nameFile
echo "average,"$avgData1","$avgData2","$avgData3","$avgData4","$avgData5","$avgData6","$avgData7","$avgData8","$avgData9","$data10","$avgData13 >> ~/log/$nameFile
```

## 3.6 Mengubah Permission File

Ketentuan selanjutnya adalah mengubah permission dari file supaya hanya dapat dibaca oleh user. Untuk melakukan hal tersebut di beri command chmod sebagai berikut.\

```bash
echo `chmod 700 ~/log/*`
echo `ls -l ~/log/*`
```

Dengan kata lain hanya user pemilik file yang bisa read, write, and execute. Sedangkan user lain tidak bisa.

## 3.7 Menjalankan Program Setiap Satu Jam Sekali

Untuk menjalankan program setiab satu jam sekali. Maka pada crontab diberikan command seperti berikut.

```bash
* */1 * * * /home/ram/Sisop/modul\ 1/praktikum/soal\ 3/aggregate_minutes_to_hourly_log.sh
```

## 3.8 Hasil

- `file minute`
  ![file minute.txt](foto/soal3_fileminute.png)
- `file hour`
  ![file hour.txt](foto/soal3_filehour.png)
- `contab every minute and hour`
  ![crontab.txt](foto/soal3_crontab.jpeg)
- `file permission`
  ![permission.txt](foto/soal3_filepermission.jpeg)

## 3.9 Kesulitan

Adapun kesulitan yang kami hadapi ketika mengerjakan soal ini yaitu:

1. Cukup kesulitan untuk mencari nilai rerata karena harus mencari beberapa data tambahan seperti total data dan jumlah file.
2. Tidak bisa menjalankan crontab script dari bash sehingga harus di setup manual.
3. Mengelolah data untuk menjadi output data sesuai dengan ketentuan cukup rumit.
