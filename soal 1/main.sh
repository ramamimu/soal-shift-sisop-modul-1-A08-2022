#!/bin/bash 

# environment yang perlu disiapkan: tidak ada

echo "Login"
echo "masukkan username"
read username
echo "masukkan password"
read -s password

countInfoLogin(){
    local data=`cat ~/log/log.txt`
    local count=0
    while read line; do
        count=$((count+1))
    done < ~/log/log.txt
    echo "Percobaan Login $count"
}

downloadImage(){
    # -d "/path/to/dir"
    local date=`date +%F`
    local folder=$date"_"$username
    if [ ! -f ./$folder.zip ]
    then
        echo "masuk ZIP"
        echo `mkdir $folder`
        for ((num=1; num<=$1; num=num+1))
        do
            # echo $num
            if [ $num -lt 10 ]
            then
                echo "MASUKKKK"
                local direktori="./$folder/PIC_0$num.jpg"
            else
                echo "MASUKKK 222"
                local direktori="./$folder/PIC_$num.jpg"
            fi
            # echo $direktori
            wget https://loremflickr.com/320/240 -O $direktori
        done
        echo `zip --password $password $folder.zip -r $folder/`
        echo `rm -r ./$folder`
    else
        echo "masuk UNZIP"
        echo `unzip -P $password $folder.zip`
        echo `rm -r $folder.zip`

        number=`ls $folder | wc -l`
        for ((num=1+number; num<=$1+number; num=num+1))
        do
            # echo $num
            if [ $num -lt 10 ]
            then
                local direktori="./$folder/PIC_0$num.jpg"
            else
                local direktori="./$folder/PIC_$num.jpg"
            fi
            # echo $direktori
            wget https://loremflickr.com/320/240 -O $direktori
        done
        echo `zip --password $password $folder.zip -r $folder/`
        echo `rm -r $folder`
    fi
}

isUserExist=false
userIndex=-2
isPassExist=false
passIndex=-2
accepted=false

if [ ! -d "./users/" ]
then
    echo `mkdir users`
    echo `touch ./users/user.txt`
fi

if [ ! -f "~/log/log.txt" ]
then
    echo `touch ~/log/log.txt`
fi

temp=`cat ./users/user.txt`
counter=0
for i in $temp 
do
    if [ $(($counter%2)) -eq 0 ]
    then
        if [ "$username" == "$i" ]
        then
            isUserExist=true
            userIndex=$counter
        fi
    fi

    if [ $(($counter%2)) -eq 1 ]
    then
        if [ "$password" == "$i" ]
        then
            isPassExist=true
            passIndex=$counter
        fi
    fi
    # echo "pass index = $passIndex and $userIndex "
    if [ $passIndex == $(($userIndex+1)) ]
    then
        accepted=true
    fi
    counter=$((counter+1))
done 

date=`date +%D`
time=`date +%T`
if [ "$accepted" == true ]
then
    echo "$date $time LOGIN: INFO User $username logged in" >> ~/log/log.txt
    echo "LOGGIN SUCCESS"
    echo "Masukkan command"
    read command
    echo $command
    if [ "$command" == "att" ]
    then
        countInfoLogin
    else
        totalImg=0
        for i in $command
        do
            totalImg=$i
        done
        # echo "total image $totalImg"
        downloadImage $totalImg
    fi
# else if [ "$isUserExist" == true ]
# di soal tidak ada opsi jika belum daftar dan mau login, jadi langsung else saja
else
    echo "$date $time LOGIN: ERROR Failed login attempt on user $username" >> ~/log/log.txt
fi