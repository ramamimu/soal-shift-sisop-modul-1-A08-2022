#!/bin/bash 
echo "Register"
echo "masukkan username"
read username
echo "masukkan password"
read -s password

cek(){
    # Syarat Pass
    # [X] Minimal 8 karakter 
    # [X] Memiliki minimal 1 huruf kapital dan 1 huruf kecil
    # [X] Alphanumeric
    # [X] Tidak boleh sama dengan username
    minChar=true
    upLoCase=false
    numeric=false
    likeUsername=false
    if [ ${#password} -lt 8 ]
    then
        echo "character must be more 8!"
        minChar=false
    fi

    if [[ $password =~ [A-Z]  &&  $password =~ [a-z] ]]
    then
        upLoCase=true
    else
        echo "character must include at least 1 UPPERCASE and lowercase!"
    fi
    if [[ $password =~ [0-9] ]]
    then
        numeric=true
    else
        echo "character must include at least 1 Alphanumeric!"
    fi
    if [ "$username" == "$password" ]
    then
        echo "username and password must be not same"
        likeUsername=true
    fi
}

cek
date=`date +%D`
time=`date +%T`
dir="./users/user.txt"

if [ ! -d "./users/" ]
then
    echo `mkdir users`
    echo `touch ./users/user.txt`
fi

if [ ! -f "~/log/log.txt" ]
then
    echo `touch ~/log/log.txt`
fi

temp=`cat ./users/user.txt`

counter=0
for i in $temp 
do
    if [ $(($counter%2)) -eq 0 ]
    then
        # echo "$i and $username"
        if [ "$username" == "$i" ]
        then
            likeUsername=true
            echo "$date $time REGISTER: ERROR User already exist" >> ~/log/log.txt
            # echo "MASUK"
        fi
    fi
    # echo $counter
    counter=$((counter+1))
done 

# success
if [[ "$minChar" = true && "$upLoCase" = true && "$numeric" = true && "$likeUsername" = false ]]
then
    echo "$date $time REGISTER: INFO User $username registered successfully" >> ~/log/log.txt
    echo -e "$username\n$password" >> ./users/user.txt
fi

# P4sswordasdas
