#!/bin/bash 

# environment yang perlu disiapkan: log_website_daffainfo.log

if [ ! -f "~/log/forensic_log_website_daffainfo_log" ]
then
    echo `mkdir ~/log/forensic_log_website_daffainfo_log`
fi

total=`awk {print} ~/log/log_website_daffainfo.log`
total=$((`echo "$total" | wc -l`-1))
# success
count=0
for ((num=0; num<=24; num=num+1))
do
    if [ $num -lt 10 ]
    then
        temp="awk '(\$1 ~ /2022:0$num:/)' ~/log/log_website_daffainfo.log"
    else
        temp="awk '(\$1 ~ /2022:$num:/)' ~/log/log_website_daffainfo.log"
    fi
    res=`eval "$temp" | wc -l`

    if [ $res -gt 0 ]
    then
        count=$((count+1))
    fi
done

mean=$(($total/12))
echo "Rata-rata serangan adalah sebanyak $mean requests per jam" >> ~/log/forensic_log_website_daffainfo_log/ratarata.txt

# CLUE SOAL C
# awk -F ':' '$1 {print $1}' log_website_daffainfo.log | sort | uniq -c | sort -rn | awk '{print $2}' | awk 'NR==1'
# awk 'NR==1' ~/log/log_website_daffainfo.log

# awk -F: '{print $2}' input.txt: using : as the delimiter, print the second field.
# | sort: sort the files so that you can then run
# | uniq -c, which compresses adjacent repeated entries into one, while listing the count -c of repeats.
# | sort -rn: sort the output by the count, in descending order.
# | awk '{print $2}': remove the count number, and just print the string that matched.

soalCIP=`awk -F ':' '$1 {print $1}' ~/log/log_website_daffainfo.log | sort | uniq -c | sort -rn | awk '{print $2}' | awk 'NR==1'`
soalCJumlah=`awk -F ':' '$1 {print $1}' ~/log/log_website_daffainfo.log | sort | uniq -c | sort -rn | awk '{print $1}' | awk 'NR==1'`

echo "IP yang paling banyak mengakses server adalah: $soalCIP sebanyak $soalCJumlah requests" >> ~/log/forensic_log_website_daffainfo_log/result.txt

countCurl=`awk '/curl/ {print}' ~/log/log_website_daffainfo.log | wc -l`
echo "Ada $countCurl requests yang menggunakan curl sebagai user-agent" >> ~/log/forensic_log_website_daffainfo_log/result.txt

# CLUE SOAL E
soalE=`awk '($1 ~ /2022:02:/)' ~/log/log_website_daffainfo.log | awk -F ':' '{print $1}' | uniq -c | awk '{print $2}'`
for i in $soalE
do
    echo $i
    echo "$i Jam 2 pagi" >> ~/log/forensic_log_website_daffainfo_log/result.txt
done