#!/bin/bash 

# environment yang perlu disiapkan: log metriks per menit metrics_<date>.log

echo "DELETING FILE PROCEED"
echo `rm ~/log/*agg*`

titleLine=`awk '{print $0}' ~/log/* | awk 'NR % 2 == 1' | awk 'NR==1'`
titleLine="type,"$titleLine
temp="awk '{print \$0}' ~/log/* | awk 'NR % 2 == 0'"
dataLine=`eval $temp`
totalData=`awk -F ',' '\$1 {print \$1}' ~/log/* | awk 'NR % 2 == 0' | wc -l`
data1="awk -F ',' '\$1 {print \$1}' ~/log/* | awk 'NR % 2 == 0'"
maxData1=`eval $data1 | sort -rn | awk 'NR==1'`
minData1=`eval $data1 | sort -n | awk 'NR==1'`
sumData1=`eval $data1 | awk '{ sum += $1 } END { print sum }'`

data2="awk -F ',' '\$1 {print \$2}' ~/log/* | awk 'NR % 2 == 0'"
maxData2=`eval $data2 | sort -rn | awk 'NR==1'`
minData2=`eval $data2 | sort -n | awk 'NR==1'`
sumData2=`eval $data2 | awk '{ sum += $1 } END { print sum }'`

data3="awk -F ',' '\$1 {print \$3}' ~/log/* | awk 'NR % 2 == 0'"
maxData3=`eval $data3 | sort -rn | awk 'NR==1'`
minData3=`eval $data3 | sort -n | awk 'NR==1'`
sumData3=`eval $data3 | awk '{ sum += $1 } END { print sum }'`

data4="awk -F ',' '\$1 {print \$4}' ~/log/* | awk 'NR % 2 == 0'"
maxData4=`eval $data4 | sort -rn | awk 'NR==1'`
minData4=`eval $data4 | sort -n | awk 'NR==1'`
sumData4=`eval $data4 | awk '{ sum += $1 } END { print sum }'`

data5="awk -F ',' '\$1 {print \$5}' ~/log/* | awk 'NR % 2 == 0'"
maxData5=`eval $data5 | sort -rn | awk 'NR==1'`
minData5=`eval $data5 | sort -n | awk 'NR==1'`
sumData5=`eval $data5 | awk '{ sum += $1 } END { print sum }'`

data6="awk -F ',' '\$1 {print \$6}' ~/log/* | awk 'NR % 2 == 0'"
maxData6=`eval $data6 | sort -rn | awk 'NR==1'`
minData6=`eval $data6 | sort -n | awk 'NR==1'`
sumData6=`eval $data6 | awk '{ sum += $1 } END { print sum }'`

data7="awk -F ',' '\$1 {print \$7}' ~/log/* | awk 'NR % 2 == 0'"
maxData7=`eval $data7 | sort -rn | awk 'NR==1'`
minData7=`eval $data7 | sort -n | awk 'NR==1'`
sumData7=`eval $data7 | awk '{ sum += $1 } END { print sum }'`

data8="awk -F ',' '\$1 {print \$8}' ~/log/* | awk 'NR % 2 == 0'"
maxData8=`eval $data8 | sort -rn | awk 'NR==1'`
minData8=`eval $data8 | sort -n | awk 'NR==1'`
sumData8=`eval $data8 | awk '{ sum += $1 } END { print sum }'`

data9="awk -F ',' '\$1 {print \$9}' ~/log/* | awk 'NR % 2 == 0'"
maxData9=`eval $data9 | sort -rn | awk 'NR==1'`
minData9=`eval $data9 | sort -n | awk 'NR==1'`
sumData9=`eval $data9 | awk '{ sum += $1 } END { print sum }'`

data10=`awk -F ',' '\$1 {print $10}' ~/log/* | awk 'NR % 2 == 0' | awk 'NR == 1'`

# perlu diolah
data11="awk -F ',' '\$1 {print \$11}' ~/log/* | awk 'NR % 2 == 0'"
# data11=`awk -F ',' '$1 {print $11}' ~/log/* | awk 'NR % 2 == 0'`
maxData11=`awk -F ',' '$1 {print $11}' ~/log/* | awk 'NR % 2 == 0' | sort -rn | awk 'NR==1'`
minData11=`awk -F ',' '$1 {print $11}' ~/log/* | awk 'NR % 2 == 0' | sort -n | awk 'NR==1'`
sumData11=`awk -F ',' '$1 {print $11}' ~/log/* | awk 'NR % 2 == 0' | awk '{ sum += $1 } END { print sum }'`
commData11=`awk -F ',' '\$1 {print \$11}' ~/log/* | awk 'NR % 2 == 0'`

data12="awk -F ',' '\$1 {print \$12}' ~/log/* | awk 'NR % 2 == 0'"
# commData12=
maxData12=`eval $data12 | sort -rn | awk 'NR==1'`
minData12=`eval $data12 | sort -n | awk 'NR==1'`
commData12=`eval $data12`


# gabungkan data 11 dan 12
# data13=`awk -F ',' '$1 {print $11"."$12}' ~/log/* | awk 'NR % 2 == 0'`
minData13=`awk -F ',' '$1 {print $11"."$12}' ~/log/* | awk 'NR % 2 == 0' | sort -n | awk 'NR==1'`
maxData13=`awk -F ',' '$1 {print $11"."$12}' ~/log/* | awk 'NR % 2 == 0' | sort -rn | awk 'NR==1'`
sumData13=`awk -F ',' '$1 {print $11"."$12}' ~/log/* | awk 'NR % 2 == 0' | grep -o '[0-9, "."]\+' | awk '{ sum += $1 } END { print sum }'`

# echo $maxData4
# echo $minData3
# echo $sumData1
# echo $dataLine
# echo $commData12
# eval $data9
# echo $data10
# eval $data11
# eval $data12
# echo $totalData
# echo $data13
# echo $minData13
# echo $maxData13
# echo $sumData13
# echo $data14
 
# rerata
avgData1=$(($sumData1/$totalData))
avgData2=$(($sumData2/$totalData))
avgData3=$(($sumData3/$totalData))
avgData4=$(($sumData4/$totalData))
avgData5=$(($sumData5/$totalData))
avgData6=$(($sumData6/$totalData))
avgData7=$(($sumData7/$totalData))
avgData8=$(($sumData8/$totalData))
avgData9=$(($sumData9/$totalData))
avgData13=$(($sumData13/$totalData))


day=`date +%d`
month=`date +%m`
year=`date +%Y`
hour=`date +%H`
minute=`date +%M`
second=`date +%S`

nameFile=$day$month$year$hour$minute$second
nameFile="metrics_agg_"$nameFile".log"
echo $titleLine >> ~/log/$nameFile
echo "minimum,"$minData1","$minData2","$minData3","$minData4","$minData5","$minData6","$minData7","$minData8","$minData9","$data10","$minData13 >> ~/log/$nameFile
echo "maximum,"$maxData1","$maxData2","$maxData3","$maxData4","$maxData5","$maxData6","$maxData7","$maxData8","$maxData9","$data10","$maxData13 >> ~/log/$nameFile
echo "average,"$avgData1","$avgData2","$avgData3","$avgData4","$avgData5","$avgData6","$avgData7","$avgData8","$avgData9","$data10","$avgData13 >> ~/log/$nameFile

echo `chmod 700 ~/log/*`
echo `ls -l ~/log/*`
# cron command 
# * */1 * * * /home/ram/Sisop/modul\ 1/praktikum/soal\ 3/aggregate_minutes_to_hourly_log.sh
# echo `crontab -l`