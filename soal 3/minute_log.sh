#!/bin/bash 

day=`date +%d`
month=`date +%m`
year=`date +%Y`
hour=`date +%H`
minute=`date +%M`
second=`date +%S`

nameFile=$day$month$year$hour$minute$second
# cekMem
if [ ! -f "~/log/metrics_$nameFile.log" ]
then
    echo `touch ~/log/metrics_$nameFile.log`
fi

memori=`free -m ~/`
echo $memori
titleMem="mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
dataMem=""
# mem="mem"
count=0
for i in $memori
do
    if [[ $count -gt 6 && $count -le 12 || $count -ge 14 ]]
    then
        dataMem=$dataMem$i","
    fi
    count=$(($count+1))
done
user=`whoami`
dataMem=$dataMem
echo $dataMem
disk=`du -sh ~/`

count=0
for i in $disk
do
    if [ $count == 0 ]
    then
        path=$i
    else
        diskData=$i
    fi
    # echo $i
    count=$(($count+1))
done
dataMem=$dataMem$diskData','$path
echo $titleMem >> ~/log/metrics_$nameFile.log
echo $dataMem >> ~/log/metrics_$nameFile.log
# command di crontab
#  * * * * * /home/ram/Sisop/modul\ 1/praktikum/soal\ 3/minute_log.sh
# mengecek melalui cronjob
# echo `crontab -l`